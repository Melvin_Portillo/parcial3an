%integracion regla de simpson 3/8 compuesta
syms x
f=log((x+1)/(x+2));
a=0;
b=1;

n=20;
h=0.05;
x0=a;
fxi=0;
fxmi=0;
for i=x0:h:b 
  xi=(i);
  if (xi~=a && xi~=b) 
    fxi=fxi+subs(f,x,(xi));
    h2=(((xi)-(xi-h))/3);
    for j=(xi-h):h2:(xi)
      xmi=(j);
      if (xmi~=(xi-h) && xmi~=xi)
        fxmi=fxmi+subs(f,x,xmi);
      end
    end 
    
  end
  if (i==b)
    for j=(xi-h):h2:(xi)
      xmi=(j);
      if (xmi~=(xi-h) && xmi~=xi)
        fxmi=fxmi+subs(f,x,xmi);
      end
    end 
  end 
end 
fx0=subs(f,x,a);
fxn=subs(f,x,b);
int=(b-a)*((fx0+(3*fxmi)+(2*fxi)+fxn)/(8*n));
fprintf('resultando de Integral= ');
disp(vpa(int,15));