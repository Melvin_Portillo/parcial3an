%integracion regla de simpson 1/3 simple
syms x
f=log((x+1)/(x+2));
a=0;
b=1;

xm=((a+b)/2);

fa=subs(f,x,a);
fb=subs(f,x,b);
fxm=subs(f,x,xm);
int=(b-a)*((fa+(4*fxm)+fb)/6);
fprintf('resultando de Integral= ');
disp(vpa(int,15));