%integracion regla del trapecio compuesta
syms x
f=log((x+1)/(x+2));
a=0;
b=1;
h=0.05;
n=20;

x0=a;
fxi=0;
for i=x0:h:b 
  xi=(i);
  if (xi~=a && xi~=b) 
    fxi=fxi+subs(f,x,(xi));
  end
end 

fx0=subs(f,x,a);
fxn=subs(f,x,b);
int=(b-a)*((fx0+(2*fxi)+fxn)/(2*n));
fprintf('resultando que Integral= ');
disp(vpa(int,15));