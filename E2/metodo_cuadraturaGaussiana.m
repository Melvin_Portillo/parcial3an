%cuadratura gaussiana

syms x

%funcion
f=log((x+1)/(x+2));
a=0;
b=1;

nivel=5;

w1=0.236926885;
w5=w1;
w2=0.478628671;
w4=w2;
w3=0.56888888;
t1=-0.906179846;
t5=(-1*(t1));
t2=-0.5384469310;
t4=(-1*(t2));
t3=0;
vcw=[w1 w2 w3 w4 w5];
vct=[t1 t2 t3 t4 t5];

fcg=0;
for i=1:1:nivel
    fit=(((b-a)*(vct(1,i))+(b+a))/2);
    fcg=(fcg+(vcw(1,i))*(subs(f,x,fit)));
end
Icg=(((b-a)/2)*(fcg));
fprintf('la integral= ');
disp(vpa(Icg,15));