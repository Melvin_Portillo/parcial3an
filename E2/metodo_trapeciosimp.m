%integracion regla del trapecio simple
syms x
f=log((x+1)/(x+2));
a=0;
b=1;

fprintf('metodo del trapecio simple.\n');
fa=subs(f,x,a);
fb=subs(f,x,b);
int=(b-a)*((fa+fb)/2);
fprintf('resultando que Integral= ');
disp(vpa(int,15));