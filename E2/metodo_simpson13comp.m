%integracion regla de simpson 1/3 compuesta
syms x
f=log((x+1)/(x+2));
a=0;
b=1;

n=20;

h=0.05;
x0=a;
fxi=0;
fxmi=0;
for i=x0:h:b 
  xi=(i);
  if (xi~=a && xi~=b) 
    fxi=fxi+subs(f,x,(xi));
    xmi=(((i-h)+i)/2);
    fxmi=fxmi+subs(f,x,xmi);
  end
  if (i==b)
    xmi=(((i-h)+i)/2);
    fxmi=fxmi+subs(f,x,xmi);
  end  
end  
fx0=subs(f,x,a);
fxn=subs(f,x,b);
int=(b-a)*((fx0+(4*fxmi)+(2*fxi)+fxn)/(6*n));
fprintf('resultando que Integral= ');
disp(vpa(int,15));