%integracion regla de simpson 3/8 simple
syms x
f=log((x+1)/(x+2));
a=0;
b=1;
  
h=0.05;

x0=a;
x1=x0+h;
x2=x1+h;
x3=x2+h;
fx0=subs(f,x,x0);
fx1=subs(f,x,x1);
fx2=subs(f,x,x2);
fx3=subs(f,x,x3);
int=(b-a)*((fx0+(3*fx1)+(3*fx2)+fx3)/8);
fprintf('resultando que Integral= ');
disp(vpa(int,15));