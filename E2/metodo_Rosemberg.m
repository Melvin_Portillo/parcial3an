%integracion metodo de rosemberg
syms x
f=log((x+1)/(x+2));
a=0;
b=1;

nivel=5;
uno=[];
dos=[];
tres=[];
cuatro=[];
cinco=[];
seis=[];
siete=[];
ocho=[];
nueve=[];
diez=[];
fh=0;
fh2=0;


for i=0:1:(nivel-1)
    h=0.05;
    for j=a:h:b
        xe=j;
        if (i==0)
            fh=fh+subs(f,x,xe);
        end
        if (i~=0)
            if (xe~=a && xe~=b)
                fh2=fh2+subs(f,x,xe);
            end
        end
    end
    
    uno=[uno vpa(((b-a)/(2^(i+1)))*(fh+(2*fh2)))];
    fh2=0;
end

fh3=0;
for i=2:1:nivel
    fh3=((4/3)*(uno(1,i))-(1/3)*(uno(1,(i-1))));
    dos=[dos fh3];
end
fh4=0;
for i=2:1:(nivel-1)
    fh4=((16/15)*(dos(1,i))-(1/15)*(dos(1,(i-1))));
    tres=[tres fh4];
end
fh5=0;
for i=2:1:(nivel-2)
    fh5=((64/63)*(tres(1,i))-(1/63)*(tres(1,(i-1))));
    cuatro=[cuatro fh5];
end
for i=2:1:(nivel-3)
    fh6=((256/255)*(cuatro(1,i))-(1/255)*(cuatro(1,(i-1))));
    cinco=[cinco fh6];
end
fprintf('resultando que Integral= ');
disp(vpa(cinco(1,1),15));

