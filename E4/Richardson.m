syms x; 
    f = cos(x) - sin(x);
    nivel = 4;
    Xo = pi()/8;
    h = 0.1;
    %Construccion de los subintervalos
    ite = 1;
    vectorh(ite) = h;
    ite = 2;
    while(ite<=nivel)
    vectorh(ite) = (vectorh(ite-1)/2);
    ite = ite +1;
    end

    %Calculo del primer nivel por medio de formula de primera diferencia
    %finita centrada

    i = 1;
    while(i<=nivel)

    Xiii = Xo + vectorh(i) + vectorh(i);
    Xii = Xo + vectorh(i);
    X1 = Xo - vectorh(i);
    X2 = Xo - vectorh(i) -vectorh(i);
    fxiii = subs(f, x, Xiii);
    fxii = subs(f, x, Xii);
    fx1 = subs(f, x, X1);
    fx2 = subs(f, x, X2);
    vector1N(i) = (-fxiii + 8*fxii -8*fx1 + fx2)/(12*vectorh(i));
    i = i + 1;
    end
    
    %Asignando el vector del primer nivel a la primera columna de la
    %matriz
    for i=1:1:nivel
    matriz(i,1) = vector1N(i);
    end

    filas = nivel - 1;
    iColumnas = 2;
    iFilas = 1;
    k = 1;
    while(iColumnas <= nivel)
    while(iFilas<=filas)
    matriz(iFilas,iColumnas) = (((4^k)*matriz((iFilas+1), (iColumnas-1)) - matriz((iFilas),(iColumnas-1)))/(4^k - 1)) ;
    iFilas = iFilas + 1;
    end
    k = k + 1;
    iColumnas = iColumnas + 1;
    filas = filas - 1;
    iFilas = 1;
    end
    disp('Respuesta: ');
    disp(vpa(matriz,15));
    disp('Aproximacion:')
    disp(vpa(matriz(nivel,1),15))
