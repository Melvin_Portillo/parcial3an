syms x y

f=1/sqrt(x+y);

% dx
a=0;
b=1;
f=(b-a)*(subs(f,x,a)+subs(f,x,b))/2;

% dy
a=1;
b=3;
f=(b-a)*(subs(f,y,a)+subs(f,y,b))/2;

disp(vpa(f,15));