syms x y

f=1/sqrt(x+y);
% dx
n=4;
a=0;
b=1;
h=(b-a)/3;

valores(1)=a;

for i=1:1:n-1
   valores(i+1)=valores(i)+h;
end

f=(b-a)*(subs(f,x,valores(1))+3*(subs(f,x,valores(2))+subs(f,x,valores(3)))+subs(f,x,valores(4)))/8;

% dy
a=1;
b=3;
h=(b-a)/3;

valores(1)=a;

for i=1:1:n-1
   valores(i+1)=valores(i)+h;
end

f=(b-a)*(subs(f,y,valores(1))+3*(subs(f,y,valores(2))+subs(f,y,valores(3)))+subs(f,y,valores(4)))/8;

disp(vpa(f,15));
