syms x y

f=1/sqrt(x+y);

% dx
n=4;
a=0;
b=1;
h=(b+a)/3;

h2=h/3;

valores(1)=a;

for i=1:1:n-1
   valores(i+1)=valores(i)+h;
end

subint(1)=valores(1)+h2;

for i=1:2:(n-2)*2
   subint(i+1)=valores(i+1)-h2;
   subint(i+2)=valores(i+1)+h2;
    
end

subint((n-2)*2+2)=valores(n)-h2;

xmi=0;
for i=1:1:(n-2)*2+2
    xmi=xmi+subs(f,x,subint(i));
end

xi=0;
for i=1:1:n-2
    xi=xi+subs(f,x,valores(i+1));
end

f=(b-a)*(subs(f,x,valores(1))+3*xmi+2*xi+subs(f,x,valores(n)))/(8*(n-1));

% dy
n=4;
a=1;
b=3;
h=(b+a)/3;

h2=h/3;

valores(1)=a;

for i=1:1:n-1
   valores(i+1)=valores(i)+h;
end

subint(1)=valores(1)+h2;

for i=1:2:(n-2)*2
   subint(i+1)=valores(i+1)-h2;
   subint(i+2)=valores(i+1)+h2;
    
end

subint((n-2)*2+2)=valores(n)-h2;

xmi=0;
for i=1:1:(n-2)*2+2
    xmi=xmi+subs(f,y,subint(i));
end

xi=0;
for i=1:1:n-2
    xi=xi+subs(f,y,valores(i+1));
end

f=(b-a)*(subs(f,y,valores(1))+3*xmi+2*xi+subs(f,y,valores(n)))/(8*(n-1));

disp(vpa(f,15));