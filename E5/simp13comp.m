syms x y

f=1/sqrt(x+y);


% dx
n=7;
a=0;
b=1;
h=(b-a)/n;

valores(1)=a;

for i=1:1:n
  valores(i+1)=valores(i)+h;
end

for i=1:1:n
   subint(i)=(valores(i)+valores(i+1))/2; 
end

xmi=0;

for i=1:1:n
    xmi=xmi+subs(f,x,subint(i));
end


xi=0;
for i=1:1:n-1
    xi=xi+subs(f,x,valores(i+1));
end

f=(b-a)*(subs(f,x,valores(1))+4*xmi+2*xi+subs(f,x,valores(n+1)))/(6*n);

% dy
n=7;
a=1;
b=3;
h=(b-a)/n;

valores(1)=a;

for i=1:1:n
  valores(i+1)=valores(i)+h;
end

for i=1:1:n
   subint(i)=(valores(i)+valores(i+1))/2; 
end

xmi=0;

for i=1:1:n
    xmi=xmi+subs(f,y,subint(i));
end


xi=0;
for i=1:1:n-1
    xi=xi+subs(f,y,valores(i+1));
end

f=(b-a)*(subs(f,y,valores(1))+4*xmi+2*xi+subs(f,y,valores(n+1)))/(6*n);

disp(vpa(f,15));

